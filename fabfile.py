from fabric.api import local, task

@task(alias='2pypi')
def distribute_to_pypi():
    """
    Distribute the eggplant to PyPI
    """
    local('python setup.py sdist upload')
    local('rm -rf django_eggplant.egg-info')
    local('rm -rf dist')
