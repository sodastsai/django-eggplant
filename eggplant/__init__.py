APP_LABEL = 'eggplant'

__version__ = '0.8.0'
VERSION = tuple(map(lambda x: int(x), __version__.split('.')))
