# https://github.com/rhec/django-html5
"""
HTML5 input widgets.
TODO: Date widgets
"""
from django.forms.widgets import Input


class HTML5Input(Input):
    use_autofocus_fallback = False
    extraAttrs = None

    def render(self, *args, **kwargs):
        attrs = kwargs['attrs']
        if self.extraAttrs is not None:
            for key, value in self.extraAttrs.items():
                attrs[key] = value
        rendered_string = super(HTML5Input, self).render(*args, **kwargs)
        # js only works when an id is set
        if self.use_autofocus_fallback and 'attrs' in kwargs and attrs.get("id", False) and 'autofocus' in attrs:
            rendered_string += """<script>
if (!("autofocus" in document.createElement("input"))) {
  document.getElementById("%s").focus();
}
</script>""" % attrs['id']
        return rendered_string


class TextInput(HTML5Input):
    input_type = 'text'


class EmailInput(HTML5Input):
    input_type = 'email'
    extraAttrs = {'spellcheck': 'false'}


class TelephoneInput(HTML5Input):
    input_type = 'tel'


class URLInput(HTML5Input):
    input_type = 'url'
    extraAttrs = {'spellcheck': 'false'}


class SearchInput(HTML5Input):
    input_type = 'search'


class ColorInput(HTML5Input):
    """
    Not supported by any browsers at this time (Jan. 2010).
    """
    input_type = 'color'


class NumberInput(HTML5Input):
    input_type = 'number'


class RangeInput(NumberInput):
    input_type = 'range'


class DateInput(HTML5Input):
    input_type = 'date'


class MonthInput(HTML5Input):
    input_type = 'month'


class WeekInput(HTML5Input):
    input_type = 'week'


class TimeInput(HTML5Input):
    input_type = 'time'


class DateTimeInput(HTML5Input):
    input_type = 'datetime'


class DateTimeLocalInput(HTML5Input):
    input_type = 'datetime-local'
