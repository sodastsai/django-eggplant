"""
Copyright 2012 Dian-Je Tsai and Wantoto Inc

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

HTTPS_ONLY = 'HTTPS_ONLY'
DEPLOYED = 'DEPLOYED'

PERMISSION_DICT = 'EGGPLANT_PERMISSION_DICT'

SIGNATURE_KEY_CLASS = 'EGGPLANT_SIGNATURE_KEY_CLASS'
AJAX_SIGNATURE_KEY_CLASS = 'EGGPLANT_AJAX_SIGNATURE_KEY_CLASS'
USER_TOKEN_CLASS = 'EGGPLANT_SIGNATURE_KEY_CLASS'
DEFAULT_AUTH_METHOD = 'EGGPLANT_API_DEFAULT_AUTH_METHOD'

MAX_PAGINATION_SIZE = 'EGGPLANT_MAX_PAGINATION_SIZE'
DEFAULT_PAGINATION_SIZE = 'EGGPLANT_DEFAULT_PAGINATION_SIZE'
