import httplib

host = 'localhost'
port = '8000'
path = '/c/'
method = 'GET'
headers = {
    'X-REQUEST-SIGNATURE': '1234'
}

conn = httplib.HTTPConnection(host)
conn.port = port
conn.request(method, path, headers=headers)
response = conn.getresponse()

print response.status
print '---'
print response.read()