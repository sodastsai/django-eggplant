from django.http import HttpResponse
from django.shortcuts import render
from eggplant.views.api import APIView

class RootView(APIView):
    def get(self):
        self.response.write('<h1>It works! class.</h1>')

def root(request):
    return HttpResponse('<h1>It works! func.</h1>')

def ajax(request):
    return render(request, 'ajax.html', {})
