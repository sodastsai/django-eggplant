from django.conf.urls import patterns, url
from eggplant_web.views import RootView

urlpatterns = patterns('',
    url(r'^c/$', RootView.view(), name='root_c'),
    url(r'^f/$', 'eggplant_web.views.root', name='root_f'),
    url(r'^a/$', 'eggplant_web.views.ajax', name='ajax'),
)