from django_eggplant.development_settings import *

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'django_eggplant',
        'USER': 'django_eggplant',
        'PASSWORD': 'django_eggplant',
        'HOST': '',
        'PORT': '',
    }
}
